# Language Change Tutorial

This repository contains preprocessed data for the tutorial on language change at NAACL and IC2S2 2019.

Tutorial notebooks and slides will be found here: https://github.com/jacobeisenstein/language-change-tutorial